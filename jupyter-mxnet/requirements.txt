keras-mxnet==2.1.6
Pillow==5.1.0
jupyter==1.0.0
matplotlib==2.2.3
tqdm==4.23.1
scikit-image==0.14.0
#!/bin/bash

tensorboard --logdir /home/tnt/logs &
python -m jupyter notebook --allow-root --ip=0.0.0.0 --port=8888
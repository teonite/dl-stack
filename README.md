### Log in to Nvidia DGX docker registry
**IMPORTANT** - without this, you won't be able to build images based on NGC registry
```bash
$ docker login nvcr.io
 
Username: $oauthtoken
Password: <Your Token>
```